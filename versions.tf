
terraform {
  required_version = ">= 0.12"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.67.0"
    }
  }
}

